package main

import (
	"context"
	"fmt"
    "math/rand"
    "strconv"
    _ "github.com/mattn/go-sqlite3"
    "database/sql"
    "os"
)

// App struct
type App struct {
	ctx context.Context
    targetNum int
    guesses int
    isActive bool
    gameId int64
    db *sql.DB
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
    a.connectToDb()
    a.startGame()
}

func (a *App) startGame() {
    a.isActive = true
    a.targetNum = a.chooseNumber()
    a.guesses = 0
    insertResult, err := a.db.Exec("insert into games (actual) values (?)", a.targetNum)
    if err != nil {
       panic(err)
    }

    a.gameId, err = insertResult.LastInsertId()
    if err != nil {
       panic(err)
    }
}

func (a *App) connectToDb() {
    dbFile := "./guess.db"
    _, err := os.Stat(dbFile)
    newDb := err != nil
    
    db, err := sql.Open("sqlite3", dbFile)
    if err != nil {
       panic(err)
    }

    if newDb {
       _, err = db.Exec("CREATE TABLE games (id integer primary key, actual integer not null)")
       if err != nil {
          db.Close()
          os.Remove(dbFile)
          panic(err)
       }
       
       _, err = db.Exec("CREATE TABLE plays (id integer primary key, game_id integer, guess integer not null, foreign key(game_id) references games(id))")
       if err != nil {
          db.Close()
          os.Remove(dbFile)
          panic(err)
       }
    }
    
    a.db = db
}

func (a *App) chooseNumber() int {
     return rand.Intn(100) + 1
}

func (a *App) Guess(value string) string {
     if !a.isActive {
        return "there is no game started. you're dumb"
     }

    g, err := strconv.Atoi(value)
    if err != nil {
       fmt.Println(err)
       return "that's not a valid number. you're so dumb"
    }

    a.guesses++
    _, err = a.db.Exec("insert into plays (game_id, guess) values (?, ?)", a.gameId, g)
    if err != nil {
       panic(err)
    }
    
    if g > 100 || g < 1 {
       return "you know it's between 1 and 100 right? I'm counting that as a guess. You're dumb."
    }
    
    if g < a.targetNum {
        return "too low"
    } else if g > a.targetNum {
        return "too high"
    } else {
      result := fmt.Sprintf("You win! only took you %d tries!", a.guesses)
      a.isActive = false
      a.guesses = 0
      return result
    }
}

func (a *App) NewGame() {
     a.startGame()
}

type Outcome struct {
     Target int
     Guesses int
}

func (a *App) GetHistory() []Outcome {
     rows, err := a.db.Query("select g.actual, count(p.id) as numGuesses from games g join plays p on p.game_id = g.id group by g.actual")
     if err != nil {
        panic(err)
     }

     defer rows.Close()

     var result = []Outcome{}
     
     for rows.Next() {
         var actual int
         var numGuesses int

         err = rows.Scan(&actual, &numGuesses)
         if err != nil {
           panic(err)
         }

         result = append(result, Outcome{actual, numGuesses})
     }
     
     return result
}
